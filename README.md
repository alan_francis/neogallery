# README #

This is a small snippet for a image gallery, this is useful for displaying folders and folders of images in a gallery in a specific order.

This uses a json file as a source to display the images.

![2017-07-02_14-16-09.gif](https://bitbucket.org/repo/XX58yr6/images/479573708-2017-07-02_14-16-09.gif)

![2017-07-02_15-38-24.jpg](https://bitbucket.org/repo/XX58yr6/images/2515679529-2017-07-02_15-38-24.jpg)

### How do I get set up? ###

Download the repository as a zip file.

Add folders into the `neogallery/` directory and make entry in `grid.json` file. This will add a grid item for the folders view.

`img/` folder has the images needed for each gallery folder

`config/` folder contains `images.json` which has the order of images to be displayed.

### Features ###

1. Each image will have it's own `location.hash` so its easy share URLs of the images.

2. Use keyboard shortcuts to navigate between images, `right arrow`, `left arrow` to move between images, `esc` key to exit the view and see the gallery.

3. You can click on the image to maximize and the click on the maximized image to return to gallery.

### Uses ###

I use this for showing off design prototypes with images, since the images are fit to width, I can see how it will look in different screens and also try out scrolling.

This can also be generated from sketch using a plugin that I am working on: [sketch-neo-preview](https://github.com/alanfraneo/sketch-neo-preview)


### LICENSE ###
MIT

### Contact ###
Have any suggestions or feedbacks? Hit me up on Twitter [@alanfraneo](https://twitter.com/alanfraneo)